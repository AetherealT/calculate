﻿using System;

namespace operation_mode
{
    public class ClassOperation : InterfaceOperation
    {
        private double first = 0;

        public void Put(double first)
        {
            this.first = first;
        }

        public double Sum(double second)
        {
            second = first + second;
            return second;
        }

        public double Subtraction(double second)
        {
            second = first - second;
            return second;
        }

        public double Multiplication(double second)
        {
            second = first * second;
            return second;
        }

        public double Division(double second)
        {
            second = first / second;
            return second;
        }

        public double Sin(double first)
        {
            return Math.Sin(first);
        }

        public double Cos(double first)
        {
            return Math.Cos(first);
        }

        public double Tg(double first)
        {
            return Math.Tan(first);
        }

        public double Ctg(double first)
        {
            return 1.0 / Math.Tan(first);
        }

        public double pow(double second)
        {
            return Math.Pow(first, second);
        }

        public double sqrt(double first)
        {
            return Math.Sqrt(first);
        }

        public double factorial(double first)
        {
            double f = 1;
            for (int i = 1; i <= first; i++)
                f *= (double)i;
            return f;

        }

        public double log(double first)
        {
            return Math.Log10(first);
        }
    }
}
