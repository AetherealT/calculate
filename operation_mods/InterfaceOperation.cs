﻿namespace operation_mode
{
    public interface InterfaceOperation
    {
        void Put(double second);
        double Multiplication(double second);
        double Division(double second);
        double Sum(double second);
        double Subtraction(double second);
        double Sin(double first);
        double Cos(double first);
        double Tg(double first);
        double Ctg(double first);
        double pow(double second);
        double sqrt(double first);
        double factorial(double first);
        double log(double first);
    }

}
