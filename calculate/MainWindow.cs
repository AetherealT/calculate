﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using operation_mode;

namespace calculate
{
    public partial class MainWindow : Form
    {
        OpenFileDialog OpenFileDialog1 = new OpenFileDialog();
        SaveFileDialog SaveFileDialog1 = new SaveFileDialog();
        Point lastPoint;
        ClassOperation operation;
        private string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        List<string> accemblyList;
        private double dollar = 73.58;
        private double mill_to_km = 1.61;
        private double euro = 83.08;
        private double euro_to_dollar = 1.13;
        public MainWindow()
        {
            InitializeComponent();
            operation = new ClassOperation();

            OpenFileDialog1.Filter = "Text Files(*.txt)|*.txt|RichText Files(*.rtf)|*.rtf|CSharp Files(*.cs)|*.cs|All files(*.*)|*.*";
            SaveFileDialog1.Filter = "Text Files(*.txt)|*.txt|RichText Files(*.rtf)|*.rtf|CSharp Files(*.cs)|*.cs|All files(*.*)|*.*";
            accemblyList = new List<string>();
        }

        private void label_close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }

        }

        private void CorrectNumber()
        {
            try
            {
                if (textBox_numbers.Text.IndexOf("∞") != -1)
                    textBox_numbers.Text = textBox_numbers.Text.Substring(0, textBox_numbers.Text.Length - 1);
                if (textBox_numbers.Text[0] == '0' && (textBox_numbers.Text.IndexOf(",") != 1))
                    textBox_numbers.Text = textBox_numbers.Text.Remove(0, 1);
                if (textBox_numbers.Text[0] == '-')
                    if (textBox_numbers.Text[1] == '0' && (textBox_numbers.Text.IndexOf(",") != 2))
                        textBox_numbers.Text = textBox_numbers.Text.Remove(1, 1);
            } catch 
            {
                MessageBox.Show("Произошла ошибка,Гриша идет домой","Ошибка");
                textBox_numbers.Text = "";
            }

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void button_c_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text = "0";
            richTextBox_text.Text = richTextBox_text.Text.Remove(richTextBox_text.Text.Length - 1);
        }

        private void button_1_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 1.ToString();
            richTextBox_text.Text += 1.ToString();
            CorrectNumber();
        }

        private void button_2_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 2.ToString();
            richTextBox_text.Text += 2.ToString();
            CorrectNumber();
        }

        private void button_3_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 3.ToString();
            richTextBox_text.Text += 3.ToString();
            CorrectNumber();
        }

        private void button_4_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 4.ToString();
            richTextBox_text.Text += 4.ToString();
            CorrectNumber();
        }

        private void button_5_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 5.ToString();
            richTextBox_text.Text += 5.ToString();
            CorrectNumber();
        }

        private void button_6_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 6.ToString();
            richTextBox_text.Text += 6.ToString();
            CorrectNumber();
        }

        private void button_7_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 7.ToString();
            richTextBox_text.Text += 7.ToString();
            CorrectNumber();
        }

        private void button_8_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 8.ToString();
            richTextBox_text.Text += 8.ToString();
            CorrectNumber();
        }

        private void button_9_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 9.ToString();
            richTextBox_text.Text += 9.ToString();
            CorrectNumber();
        }

        private void button_0_Click(object sender, EventArgs e)
        {
            textBox_numbers.Text += 0.ToString();
            richTextBox_text.Text += 0.ToString();
            CorrectNumber();
        }

        private string ToN(string number, string sys)
        {
            string newNum = "";
            int num = Convert.ToInt32(number);
            int chast = Convert.ToInt32(number);
            ArrayList numTemp = new ArrayList();
            while (chast > 0)
            {
                chast = chast / Convert.ToInt32(sys);
                numTemp.Add(num - chast * Convert.ToInt32(sys));
                num = chast;
            }
            int j;
            for (j = numTemp.Count - 1; j >= 0; j--)
                newNum += newCh(numTemp[j].ToString(), "to");
            return newNum;
        }
    
        string newCh(string sym, string responce)
        {
            string s = "";
            if (responce == "to")
            {
                if (Convert.ToInt32(sym) > 10)
                    s += alphabet.Substring(Convert.ToInt32(sym) - 10, 1);
                else
                    s += sym;
            }
            else if (responce == "from")
            {
                if (alphabet.IndexOf(sym) == -1)
                    s += sym;
                else
                    s += (alphabet.IndexOf(sym) + 10).ToString();
            }

            if (s == 10.ToString())
                s = "A";
            
            return s;
        }



        private void button_res_Click(object sender, EventArgs e)
        {
            try
            {
                if (!button_pow.Enabled)
                {
                    textBox_numbers.Text = operation.pow(Convert.ToDouble(textBox_numbers.Text)).ToString();
                    richTextBox_text.Text += "=" + textBox_numbers.Text + "\n";
                }

                if (!button_umn.Enabled)
                {
                    textBox_numbers.Text = operation.Multiplication(Convert.ToDouble(textBox_numbers.Text)).ToString();
                    richTextBox_text.Text += "=" + textBox_numbers.Text + "\n";
                }

                if (!button_del.Enabled)
                {
                    textBox_numbers.Text = operation.Division(Convert.ToDouble(textBox_numbers.Text)).ToString();
                    richTextBox_text.Text += "=" + textBox_numbers.Text + "\n";
                }

                if (!button_plus.Enabled)
                {
                    textBox_numbers.Text = operation.Sum(Convert.ToDouble(textBox_numbers.Text)).ToString();
                    richTextBox_text.Text += "=" + textBox_numbers.Text + "\n";
                }


                if (!button_min.Enabled)
                {
                    textBox_numbers.Text = operation.Subtraction(Convert.ToDouble(textBox_numbers.Text)).ToString();
                    richTextBox_text.Text += "=" + textBox_numbers.Text + "\n";
                }

                if (Convert.ToDouble(textBox_numbers.Text) % 2 == 0)
                {
                    label_dec.Text = textBox_numbers.Text;
                    octo.Text = ToN(textBox_numbers.Text, 8.ToString());
                    hex.Text = ToN(textBox_numbers.Text, 16.ToString());
                    binary.Text = ToN(textBox_numbers.Text, 2.ToString());
                }


                FreeButtons();
            } catch
            {
                MessageBox.Show("Ваш ответ не является числом","Ошибка");
            }


        }

        private void button_plus_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                operation.Put(Convert.ToDouble(textBox_numbers.Text));
                richTextBox_text.Text += "+";
                button_plus.Enabled = false;
                textBox_numbers.Text = "0";
            }
        }

        private void button_min_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                operation.Put(Convert.ToDouble(textBox_numbers.Text));
                button_min.Enabled = false;
                richTextBox_text.Text += "-";
                textBox_numbers.Text = "0";
            }
        }

        private void button_del_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                operation.Put(Convert.ToDouble(textBox_numbers.Text));
                richTextBox_text.Text += "/";
                button_del.Enabled = false;
                textBox_numbers.Text = "0";
            }
        }

        private void button_umn_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                operation.Put(Convert.ToDouble(textBox_numbers.Text));
                richTextBox_text.Text += "*";
                button_umn.Enabled = false;
                textBox_numbers.Text = "0";
            }
        }

        private void FreeButtons()
        {
            button_min.Enabled = true;
            button_del.Enabled = true;
            button_plus.Enabled = true;
            button_umn.Enabled = true;
            button_pow.Enabled = true;
        }

        private bool CanPress()
        {
            if (!button_umn.Enabled)
                return false;

            if (!button_pow.Enabled)
                return false;

            if (!button_sin.Enabled)
                return false;

            if (!button_cos.Enabled)
                return false;

            if (!button_tn.Enabled)
                return false;

            if (!button_ctg.Enabled)
                return false;

            if (!button_del.Enabled)
                return false;

            if (!button_plus.Enabled)
                return false;

            if (!button_min.Enabled)
                return false;

            return true;
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            richTextBox_text.Text += "\nsin(" + textBox_numbers.Text +") = ";
            textBox_numbers.Text = operation.Sin(Convert.ToDouble(textBox_numbers.Text)).ToString();
            richTextBox_text.Text += textBox_numbers.Text + "\n";
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            richTextBox_text.Text += "\ncos(" + textBox_numbers.Text + ") = ";
            textBox_numbers.Text = operation.Cos(Convert.ToDouble(textBox_numbers.Text)).ToString();
            richTextBox_text.Text += textBox_numbers.Text + "\n";
        }

        private void button_tn_Click(object sender, EventArgs e)
        {
            richTextBox_text.Text += "\ntg(" + textBox_numbers.Text + ") = ";
            textBox_numbers.Text = operation.Ctg(Convert.ToDouble(textBox_numbers.Text)).ToString();
            richTextBox_text.Text += textBox_numbers.Text + "\n";
        }

        private void button_ctg_Click(object sender, EventArgs e)
        {
            richTextBox_text.Text += "\nctg(" + textBox_numbers.Text + ") = ";
            textBox_numbers.Text = operation.Tg(Convert.ToDouble(textBox_numbers.Text)).ToString();
            richTextBox_text.Text += textBox_numbers.Text + "\n";
        }

        private void button_log10_Click(object sender, EventArgs e)
        {
            richTextBox_text.Text += "\nlog10(" + textBox_numbers.Text + ") = ";
            textBox_numbers.Text = operation.log(Convert.ToDouble(textBox_numbers.Text)).ToString();
            richTextBox_text.Text += textBox_numbers.Text + "\n";
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            richTextBox_text.Text += "\nsqrt(" + textBox_numbers.Text + ") = ";
            textBox_numbers.Text = operation.sqrt(Convert.ToDouble(textBox_numbers.Text)).ToString();
            richTextBox_text.Text += textBox_numbers.Text + "\n";
        }

        private void button_fact_Click(object sender, EventArgs e)
        {
            richTextBox_text.Text += "\nfact(" + textBox_numbers.Text + ") = ";
            textBox_numbers.Text = operation.factorial(Convert.ToDouble(textBox_numbers.Text)).ToString();
            richTextBox_text.Text +=textBox_numbers.Text + "\n";
        }

        private void button_pow_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                richTextBox_text.Text += "\n" + textBox_numbers.Text + "^";
                operation.Put(Convert.ToDouble(textBox_numbers.Text));
                button_pow.Enabled = false;
                textBox_numbers.Text = "0";
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button_val_Click(object sender, EventArgs e)
        {
            if(listBox__VAL1.SelectedItem == "Евро" && listBox__VAL2.SelectedItem == "Евро")
            {
                textBox_V2.Text = textBox_V1.Text;
            }

            if (listBox__VAL1.SelectedItem == "Рубль" && listBox__VAL2.SelectedItem == "Рубль")
            {
                textBox_V2.Text = textBox_V1.Text;
            }

            if (listBox__VAL1.SelectedItem == "Доллар" && listBox__VAL2.SelectedItem == "Доллар")
            {
                textBox_V2.Text = textBox_V1.Text;
            }

            if (listBox__VAL1.SelectedItem == "Евро" && listBox__VAL2.SelectedItem == "Рубль")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = euro * x;
                textBox_V2.Text = z.ToString();
            }

            if (listBox__VAL1.SelectedItem == "Доллар" && listBox__VAL2.SelectedItem == "Рубль")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = dollar * x;
                textBox_V2.Text = z.ToString();
            }

            if (listBox__VAL1.SelectedItem == "Рубль" && listBox__VAL2.SelectedItem == "Евро")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = x / euro ;
                textBox_V2.Text = z.ToString();
            }

            if (listBox__VAL1.SelectedItem == "Рубль" && listBox__VAL2.SelectedItem == "Доллар")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = x / dollar;
                textBox_V2.Text = z.ToString();
            }

            if (listBox__VAL1.SelectedItem == "Евро" && listBox__VAL2.SelectedItem == "Рубль")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = euro * x;
                textBox_V2.Text = z.ToString();
            }

            if (listBox__VAL1.SelectedItem == "Доллар" && listBox__VAL2.SelectedItem == "Рубль")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = dollar * x;
                textBox_V2.Text = z.ToString();
            }

            if (listBox__VAL1.SelectedItem == "Евро" && listBox__VAL2.SelectedItem == "Доллар")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = x * euro_to_dollar;
                textBox_V2.Text = z.ToString();
            }

            if (listBox__VAL1.SelectedItem == "Доллар" && listBox__VAL2.SelectedItem == "Евро")
            {
                double x = Convert.ToDouble(textBox_V1.Text);
                double z = x / euro_to_dollar;
                textBox_V2.Text = z.ToString();
            }
        }

        private void button_speed_Click(object sender, EventArgs e)
        {
            if (listBox__U1.SelectedItem == "Милли в час" && listBox__U2.SelectedItem == "Милли в час")
            {
                textBox_U2.Text = textBox_U1.Text;
            }

            if (listBox__U1.SelectedItem == "Км в час" && listBox__U2.SelectedItem == "Км в час")
            {
                textBox_U2.Text = textBox_U1.Text;
            }

            if (listBox__U1.SelectedItem == "Км в час" && listBox__U2.SelectedItem == "Милли в час")
            {
                double x = Convert.ToDouble(textBox_U1.Text);
                double z = x / mill_to_km;
                textBox_U2.Text = z.ToString();
            }

            if (listBox__U1.SelectedItem == "Милли в час" && listBox__U2.SelectedItem == "Км в час")
            {
                double x = Convert.ToDouble(textBox_U1.Text);
                double z = x * mill_to_km;
                textBox_U2.Text = z.ToString();
            }
        }

        private void button_time_Click(object sender, EventArgs e)
        {
            if(listBox_T1.SelectedItem == "Часы" && listBox_T1.SelectedItem == "Секунды")
            {
                double k = Convert.ToDouble(textBox_T1.Text) * 3600;
                textBox_T2.Text = k.ToString();
            }

            if (listBox_T1.SelectedItem == "Часы" && listBox_T1.SelectedItem == "Минуты")
            {
                double k = Convert.ToDouble(textBox_T1.Text) * 60;
                textBox_T2.Text = k.ToString();
            }

            if (listBox_T1.SelectedItem == "Минуты" && listBox_T1.SelectedItem == "Секунды")
            {
                double k = Convert.ToDouble(textBox_T1.Text) * 60;
                textBox_T2.Text = k.ToString();
            }

            if (listBox_T1.SelectedItem == "Минуты" && listBox_T1.SelectedItem == "Часы")
            {
                double k = Convert.ToDouble(textBox_T1.Text) / 60;
                textBox_T2.Text = k.ToString();
            }

            if (listBox_T1.SelectedItem == "Секунды" && listBox_T1.SelectedItem == "Минуты")
            {
                double k = Convert.ToDouble(textBox_T1.Text) / 60;
                textBox_T2.Text = k.ToString();
            }

            if (listBox_T1.SelectedItem == "Cекунды" && listBox_T1.SelectedItem == "Часы")
            {
                double k = Convert.ToDouble(textBox_T1.Text) / 3600;
                textBox_T2.Text = k.ToString();
            }
        }

        private void button_s_Click(object sender, EventArgs e)
        {
            if(listBox_S1.SelectedItem == "Сантиметры" && listBox_S2.SelectedItem == "Сантиметры")
            {
                textBox_S2.Text = textBox_S1.Text;
            }

            if (listBox_S1.SelectedItem == "Сантиметры" && listBox_S2.SelectedItem == "Дециметры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) / 10;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Сантиметры" && listBox_S2.SelectedItem == "Метры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) / 100;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Сантиметры" && listBox_S2.SelectedItem == "Километры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) / 100000;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Километры" && listBox_S2.SelectedItem == "Сантиметры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) * 100000;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Километры" && listBox_S2.SelectedItem == "Дециметры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) * 10000;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Километры" && listBox_S2.SelectedItem == "Метры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) * 1000;
                textBox_S2.Text = k.ToString();
            }
         

            if (listBox_S1.SelectedItem == "Дециметры" && listBox_S2.SelectedItem == "Дециметры")
            {
                textBox_S2.Text = textBox_S1.Text;
            }


            if (listBox_S1.SelectedItem == "Дециметры" && listBox_S2.SelectedItem == "Сантиметр")
            {
                double k = Convert.ToDouble(textBox_S1.Text) * 10;
                textBox_S2.Text = k.ToString();
            }


            if (listBox_S1.SelectedItem == "Дециметры" && listBox_S2.SelectedItem == "Метры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) / 10;
                textBox_S2.Text = k.ToString();
            }


            if (listBox_S1.SelectedItem == "Дециметры" && listBox_S2.SelectedItem == "Километры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) / 10000;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Километры" && listBox_S2.SelectedItem == "Километры")
            {
                textBox_S2.Text = textBox_S1.Text;
            }

            if (listBox_S1.SelectedItem == "Метры" && listBox_S2.SelectedItem == "Метры")
            {
                textBox_S2.Text = textBox_S1.Text;
            }

            if (listBox_S1.SelectedItem == "Метры" && listBox_S2.SelectedItem == "Сантиметры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) * 100;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Метры" && listBox_S2.SelectedItem == "Дециметры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) * 10;
                textBox_S2.Text = k.ToString();
            }

            if (listBox_S1.SelectedItem == "Метры" && listBox_S2.SelectedItem == "Километры")
            {
                double k = Convert.ToDouble(textBox_S1.Text) / 1000;
                textBox_S2.Text = k.ToString();
            }

        }

        private void button_open_Click(object sender, EventArgs e)
        {
            if (OpenFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string filename = OpenFileDialog1.FileName;

            if (filename.EndsWith("txt") || filename.EndsWith("cs"))
            {
                richTextBox_text.LoadFile(filename, RichTextBoxStreamType.PlainText);
            }
            else if (filename.EndsWith("rtf"))
            {
                richTextBox_text.LoadFile(filename, RichTextBoxStreamType.RichText);
            }
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            if (SaveFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string filename = SaveFileDialog1.FileName;

            if (filename.EndsWith("txt") || filename.EndsWith("cs"))
            {
                richTextBox_text.SaveFile(filename, RichTextBoxStreamType.PlainText);
            }
            else if (filename.EndsWith("rtf"))
            {
                richTextBox_text.SaveFile(filename, RichTextBoxStreamType.RichText);
            }
        }

        private void button_v_Click(object sender, EventArgs e)
        {
            if(listBox_V1.SelectedItem == "Миллилитры" && listBox_V2.SelectedItem == "Миллилитры")
            {
                textBox_VV2.Text = textBox_VV1.Text;
            }

            if (listBox_V1.SelectedItem == "Миллилитры" && listBox_V2.SelectedItem == "Литры")
            {
                double k = Convert.ToDouble(textBox_VV1.Text) / 1000;
                textBox_VV2.Text = k.ToString();
            }

            if (listBox_V1.SelectedItem == "Литры" && listBox_V2.SelectedItem == "Литры")
            {
                textBox_VV2.Text = textBox_VV1.Text;
            }


            if (listBox_V1.SelectedItem == "Литры" && listBox_V2.SelectedItem == "Миллилитры")
            {
                double k = Convert.ToDouble(textBox_VV1.Text) * 1000;
                textBox_VV2.Text = k.ToString();
            }
        }

        private void button_grad_Click(object sender, EventArgs e)
        {
            if (listBox_G1.SelectedItem == "Цельсий" && listBox_G2.SelectedItem == "Цельсий")
            {
                textBox_G2.Text = textBox_G1.Text;
            }

            if (listBox_G1.SelectedItem == "Цельсий" && listBox_G2.SelectedItem == "Форенгейт")
            {
                double k = Convert.ToDouble(textBox_G1.Text) * 9 / 5 + 32;
                textBox_G2.Text = k.ToString();
            }

            if (listBox_G1.SelectedItem == "Форенгейт" && listBox_G2.SelectedItem == "Форенгейт")
            {
                textBox_G2.Text = textBox_G1.Text;
            }


            if (listBox_G1.SelectedItem == "Форенгейт" && listBox_G2.SelectedItem == "Цельсий")
            {

                double k = (5 * (Convert.ToDouble(textBox_G1.Text) - 32))/9;
                textBox_G2.Text = k.ToString();
            }
        }

        private void button_F_Click(object sender, EventArgs e)
        {

        }

        private void button_H_Click(object sender, EventArgs e)
        {
            if (listBox_H1.SelectedItem == "Граммы" && listBox_H2.SelectedItem == "Граммы")
            {
                textBox_H2.Text = textBox_H1.Text;
            }

            if (listBox_H1.SelectedItem == "Граммы" && listBox_H2.SelectedItem == "Килограммы")
            {
                double k = Convert.ToDouble(textBox_H1.Text) / 1000;
                textBox_H2.Text = textBox_H1.Text;
            }

            if (listBox_H1.SelectedItem == "Килограммы" && listBox_H2.SelectedItem == "Килограммы")
            {
                double k = Convert.ToDouble(textBox_H1.Text) * 1000;
                textBox_H2.Text = textBox_H1.Text;
            }
        }
    }
}
