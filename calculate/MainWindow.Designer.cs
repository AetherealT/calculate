﻿namespace calculate
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_hide = new System.Windows.Forms.Label();
            this.label_close = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label_name = new System.Windows.Forms.Label();
            this.button_1 = new System.Windows.Forms.Button();
            this.button_2 = new System.Windows.Forms.Button();
            this.button_3 = new System.Windows.Forms.Button();
            this.button_4 = new System.Windows.Forms.Button();
            this.button_5 = new System.Windows.Forms.Button();
            this.button_6 = new System.Windows.Forms.Button();
            this.button_7 = new System.Windows.Forms.Button();
            this.button_8 = new System.Windows.Forms.Button();
            this.button_9 = new System.Windows.Forms.Button();
            this.button_plus = new System.Windows.Forms.Button();
            this.button_min = new System.Windows.Forms.Button();
            this.button_del = new System.Windows.Forms.Button();
            this.button_0 = new System.Windows.Forms.Button();
            this.button_umn = new System.Windows.Forms.Button();
            this.button_res = new System.Windows.Forms.Button();
            this.button_c = new System.Windows.Forms.Button();
            this.textBox_numbers = new System.Windows.Forms.TextBox();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.button_tn = new System.Windows.Forms.Button();
            this.button_ctg = new System.Windows.Forms.Button();
            this.button_log10 = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.button_fact = new System.Windows.Forms.Button();
            this.button_pow = new System.Windows.Forms.Button();
            this.label_dec = new System.Windows.Forms.Label();
            this.binary = new System.Windows.Forms.Label();
            this.octo = new System.Windows.Forms.Label();
            this.hex = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.button_H = new System.Windows.Forms.Button();
            this.listBox_H1 = new System.Windows.Forms.ListBox();
            this.textBox_H2 = new System.Windows.Forms.TextBox();
            this.textBox_H1 = new System.Windows.Forms.TextBox();
            this.listBox_H2 = new System.Windows.Forms.ListBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button_F = new System.Windows.Forms.Button();
            this.listBox_F1 = new System.Windows.Forms.ListBox();
            this.textBox_F2 = new System.Windows.Forms.TextBox();
            this.textBox_F1 = new System.Windows.Forms.TextBox();
            this.listBox_F2 = new System.Windows.Forms.ListBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button_grad = new System.Windows.Forms.Button();
            this.listBox_G1 = new System.Windows.Forms.ListBox();
            this.textBox_G2 = new System.Windows.Forms.TextBox();
            this.textBox_G1 = new System.Windows.Forms.TextBox();
            this.listBox_G2 = new System.Windows.Forms.ListBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button_v = new System.Windows.Forms.Button();
            this.listBox_V1 = new System.Windows.Forms.ListBox();
            this.textBox_VV2 = new System.Windows.Forms.TextBox();
            this.textBox_VV1 = new System.Windows.Forms.TextBox();
            this.listBox_V2 = new System.Windows.Forms.ListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button_speed = new System.Windows.Forms.Button();
            this.listBox__U1 = new System.Windows.Forms.ListBox();
            this.textBox_U2 = new System.Windows.Forms.TextBox();
            this.textBox_U1 = new System.Windows.Forms.TextBox();
            this.listBox__U2 = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button_time = new System.Windows.Forms.Button();
            this.listBox_T1 = new System.Windows.Forms.ListBox();
            this.textBox_T2 = new System.Windows.Forms.TextBox();
            this.textBox_T1 = new System.Windows.Forms.TextBox();
            this.listBox_T2 = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button_val = new System.Windows.Forms.Button();
            this.listBox__VAL1 = new System.Windows.Forms.ListBox();
            this.textBox_V2 = new System.Windows.Forms.TextBox();
            this.textBox_V1 = new System.Windows.Forms.TextBox();
            this.listBox__VAL2 = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_s = new System.Windows.Forms.Button();
            this.listBox_S1 = new System.Windows.Forms.ListBox();
            this.textBox_S2 = new System.Windows.Forms.TextBox();
            this.textBox_S1 = new System.Windows.Forms.TextBox();
            this.listBox_S2 = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.richTextBox_text = new System.Windows.Forms.RichTextBox();
            this.button_save = new System.Windows.Forms.Button();
            this.button_open = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label_hide);
            this.panel1.Controls.Add(this.label_close);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label_name);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(862, 27);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // label_hide
            // 
            this.label_hide.AutoSize = true;
            this.label_hide.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_hide.ForeColor = System.Drawing.Color.White;
            this.label_hide.Location = new System.Drawing.Point(823, 0);
            this.label_hide.Name = "label_hide";
            this.label_hide.Size = new System.Drawing.Size(19, 25);
            this.label_hide.TabIndex = 4;
            this.label_hide.Text = "-";
            this.label_hide.Click += new System.EventHandler(this.label6_Click);
            // 
            // label_close
            // 
            this.label_close.AutoSize = true;
            this.label_close.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_close.ForeColor = System.Drawing.Color.White;
            this.label_close.Location = new System.Drawing.Point(842, 0);
            this.label_close.Name = "label_close";
            this.label_close.Size = new System.Drawing.Size(20, 20);
            this.label_close.TabIndex = 1;
            this.label_close.Text = "X";
            this.label_close.Click += new System.EventHandler(this.label_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 21);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_name.ForeColor = System.Drawing.Color.White;
            this.label_name.Location = new System.Drawing.Point(21, 7);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(173, 16);
            this.label_name.TabIndex = 2;
            this.label_name.Text = "Калькулятор (Качкуркин)";
            // 
            // button_1
            // 
            this.button_1.BackColor = System.Drawing.Color.White;
            this.button_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_1.Location = new System.Drawing.Point(6, 19);
            this.button_1.Name = "button_1";
            this.button_1.Size = new System.Drawing.Size(70, 70);
            this.button_1.TabIndex = 1;
            this.button_1.Text = "1";
            this.button_1.UseVisualStyleBackColor = false;
            this.button_1.Click += new System.EventHandler(this.button_1_Click);
            // 
            // button_2
            // 
            this.button_2.BackColor = System.Drawing.Color.White;
            this.button_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_2.Location = new System.Drawing.Point(82, 19);
            this.button_2.Name = "button_2";
            this.button_2.Size = new System.Drawing.Size(70, 70);
            this.button_2.TabIndex = 2;
            this.button_2.Text = "2";
            this.button_2.UseVisualStyleBackColor = false;
            this.button_2.Click += new System.EventHandler(this.button_2_Click);
            // 
            // button_3
            // 
            this.button_3.BackColor = System.Drawing.Color.White;
            this.button_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_3.Location = new System.Drawing.Point(158, 19);
            this.button_3.Name = "button_3";
            this.button_3.Size = new System.Drawing.Size(70, 70);
            this.button_3.TabIndex = 3;
            this.button_3.Text = "3";
            this.button_3.UseVisualStyleBackColor = false;
            this.button_3.Click += new System.EventHandler(this.button_3_Click);
            // 
            // button_4
            // 
            this.button_4.BackColor = System.Drawing.Color.White;
            this.button_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_4.Location = new System.Drawing.Point(6, 95);
            this.button_4.Name = "button_4";
            this.button_4.Size = new System.Drawing.Size(70, 70);
            this.button_4.TabIndex = 4;
            this.button_4.Text = "4";
            this.button_4.UseVisualStyleBackColor = false;
            this.button_4.Click += new System.EventHandler(this.button_4_Click);
            // 
            // button_5
            // 
            this.button_5.BackColor = System.Drawing.Color.White;
            this.button_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_5.Location = new System.Drawing.Point(82, 95);
            this.button_5.Name = "button_5";
            this.button_5.Size = new System.Drawing.Size(70, 70);
            this.button_5.TabIndex = 5;
            this.button_5.Text = "5";
            this.button_5.UseVisualStyleBackColor = false;
            this.button_5.Click += new System.EventHandler(this.button_5_Click);
            // 
            // button_6
            // 
            this.button_6.BackColor = System.Drawing.Color.White;
            this.button_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_6.Location = new System.Drawing.Point(158, 95);
            this.button_6.Name = "button_6";
            this.button_6.Size = new System.Drawing.Size(70, 70);
            this.button_6.TabIndex = 6;
            this.button_6.Text = "6";
            this.button_6.UseVisualStyleBackColor = false;
            this.button_6.Click += new System.EventHandler(this.button_6_Click);
            // 
            // button_7
            // 
            this.button_7.BackColor = System.Drawing.Color.White;
            this.button_7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_7.Location = new System.Drawing.Point(6, 171);
            this.button_7.Name = "button_7";
            this.button_7.Size = new System.Drawing.Size(70, 70);
            this.button_7.TabIndex = 7;
            this.button_7.Text = "7";
            this.button_7.UseVisualStyleBackColor = false;
            this.button_7.Click += new System.EventHandler(this.button_7_Click);
            // 
            // button_8
            // 
            this.button_8.BackColor = System.Drawing.Color.White;
            this.button_8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_8.Location = new System.Drawing.Point(82, 171);
            this.button_8.Name = "button_8";
            this.button_8.Size = new System.Drawing.Size(70, 70);
            this.button_8.TabIndex = 8;
            this.button_8.Text = "8";
            this.button_8.UseVisualStyleBackColor = false;
            this.button_8.Click += new System.EventHandler(this.button_8_Click);
            // 
            // button_9
            // 
            this.button_9.BackColor = System.Drawing.Color.White;
            this.button_9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_9.Location = new System.Drawing.Point(158, 171);
            this.button_9.Name = "button_9";
            this.button_9.Size = new System.Drawing.Size(70, 70);
            this.button_9.TabIndex = 9;
            this.button_9.Text = "9";
            this.button_9.UseVisualStyleBackColor = false;
            this.button_9.Click += new System.EventHandler(this.button_9_Click);
            // 
            // button_plus
            // 
            this.button_plus.BackColor = System.Drawing.Color.White;
            this.button_plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_plus.Location = new System.Drawing.Point(234, 19);
            this.button_plus.Name = "button_plus";
            this.button_plus.Size = new System.Drawing.Size(70, 70);
            this.button_plus.TabIndex = 11;
            this.button_plus.Text = "+";
            this.button_plus.UseVisualStyleBackColor = false;
            this.button_plus.Click += new System.EventHandler(this.button_plus_Click);
            // 
            // button_min
            // 
            this.button_min.BackColor = System.Drawing.Color.White;
            this.button_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_min.Location = new System.Drawing.Point(234, 95);
            this.button_min.Name = "button_min";
            this.button_min.Size = new System.Drawing.Size(70, 70);
            this.button_min.TabIndex = 12;
            this.button_min.Text = "-";
            this.button_min.UseVisualStyleBackColor = false;
            this.button_min.Click += new System.EventHandler(this.button_min_Click);
            // 
            // button_del
            // 
            this.button_del.BackColor = System.Drawing.Color.White;
            this.button_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_del.Location = new System.Drawing.Point(234, 171);
            this.button_del.Name = "button_del";
            this.button_del.Size = new System.Drawing.Size(70, 70);
            this.button_del.TabIndex = 13;
            this.button_del.Text = "/";
            this.button_del.UseVisualStyleBackColor = false;
            this.button_del.Click += new System.EventHandler(this.button_del_Click);
            // 
            // button_0
            // 
            this.button_0.BackColor = System.Drawing.Color.White;
            this.button_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_0.Location = new System.Drawing.Point(82, 247);
            this.button_0.Name = "button_0";
            this.button_0.Size = new System.Drawing.Size(70, 70);
            this.button_0.TabIndex = 14;
            this.button_0.Text = "0";
            this.button_0.UseVisualStyleBackColor = false;
            this.button_0.Click += new System.EventHandler(this.button_0_Click);
            // 
            // button_umn
            // 
            this.button_umn.BackColor = System.Drawing.Color.White;
            this.button_umn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_umn.Location = new System.Drawing.Point(234, 247);
            this.button_umn.Name = "button_umn";
            this.button_umn.Size = new System.Drawing.Size(70, 70);
            this.button_umn.TabIndex = 15;
            this.button_umn.Text = "*";
            this.button_umn.UseVisualStyleBackColor = false;
            this.button_umn.Click += new System.EventHandler(this.button_umn_Click);
            // 
            // button_res
            // 
            this.button_res.BackColor = System.Drawing.Color.White;
            this.button_res.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_res.Location = new System.Drawing.Point(158, 247);
            this.button_res.Name = "button_res";
            this.button_res.Size = new System.Drawing.Size(70, 70);
            this.button_res.TabIndex = 16;
            this.button_res.Text = "=";
            this.button_res.UseVisualStyleBackColor = false;
            this.button_res.Click += new System.EventHandler(this.button_res_Click);
            // 
            // button_c
            // 
            this.button_c.BackColor = System.Drawing.Color.White;
            this.button_c.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_c.Location = new System.Drawing.Point(6, 247);
            this.button_c.Name = "button_c";
            this.button_c.Size = new System.Drawing.Size(70, 70);
            this.button_c.TabIndex = 17;
            this.button_c.Text = "C";
            this.button_c.UseVisualStyleBackColor = false;
            this.button_c.Click += new System.EventHandler(this.button_c_Click);
            // 
            // textBox_numbers
            // 
            this.textBox_numbers.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_numbers.Location = new System.Drawing.Point(6, 6);
            this.textBox_numbers.Name = "textBox_numbers";
            this.textBox_numbers.Size = new System.Drawing.Size(819, 47);
            this.textBox_numbers.TabIndex = 18;
            this.textBox_numbers.Text = "0";
            // 
            // button_sin
            // 
            this.button_sin.BackColor = System.Drawing.Color.White;
            this.button_sin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_sin.Location = new System.Drawing.Point(310, 19);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(70, 70);
            this.button_sin.TabIndex = 20;
            this.button_sin.Text = "sin";
            this.button_sin.UseVisualStyleBackColor = false;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_cos
            // 
            this.button_cos.BackColor = System.Drawing.Color.White;
            this.button_cos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_cos.Location = new System.Drawing.Point(310, 95);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(70, 70);
            this.button_cos.TabIndex = 21;
            this.button_cos.Text = "cos";
            this.button_cos.UseVisualStyleBackColor = false;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // button_tn
            // 
            this.button_tn.BackColor = System.Drawing.Color.White;
            this.button_tn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_tn.Location = new System.Drawing.Point(310, 171);
            this.button_tn.Name = "button_tn";
            this.button_tn.Size = new System.Drawing.Size(70, 70);
            this.button_tn.TabIndex = 22;
            this.button_tn.Text = "tg";
            this.button_tn.UseVisualStyleBackColor = false;
            this.button_tn.Click += new System.EventHandler(this.button_tn_Click);
            // 
            // button_ctg
            // 
            this.button_ctg.BackColor = System.Drawing.Color.White;
            this.button_ctg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_ctg.Location = new System.Drawing.Point(310, 247);
            this.button_ctg.Name = "button_ctg";
            this.button_ctg.Size = new System.Drawing.Size(70, 70);
            this.button_ctg.TabIndex = 23;
            this.button_ctg.Text = "ctg";
            this.button_ctg.UseVisualStyleBackColor = false;
            this.button_ctg.Click += new System.EventHandler(this.button_ctg_Click);
            // 
            // button_log10
            // 
            this.button_log10.BackColor = System.Drawing.Color.White;
            this.button_log10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_log10.Location = new System.Drawing.Point(386, 247);
            this.button_log10.Name = "button_log10";
            this.button_log10.Size = new System.Drawing.Size(70, 70);
            this.button_log10.TabIndex = 27;
            this.button_log10.Text = "log10";
            this.button_log10.UseVisualStyleBackColor = false;
            this.button_log10.Click += new System.EventHandler(this.button_log10_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.BackColor = System.Drawing.Color.White;
            this.button_sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_sqrt.Location = new System.Drawing.Point(387, 172);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(70, 70);
            this.button_sqrt.TabIndex = 26;
            this.button_sqrt.Text = "√";
            this.button_sqrt.UseVisualStyleBackColor = false;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // button_fact
            // 
            this.button_fact.BackColor = System.Drawing.Color.White;
            this.button_fact.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_fact.Location = new System.Drawing.Point(386, 95);
            this.button_fact.Name = "button_fact";
            this.button_fact.Size = new System.Drawing.Size(70, 70);
            this.button_fact.TabIndex = 25;
            this.button_fact.Text = "!";
            this.button_fact.UseVisualStyleBackColor = false;
            this.button_fact.Click += new System.EventHandler(this.button_fact_Click);
            // 
            // button_pow
            // 
            this.button_pow.BackColor = System.Drawing.Color.White;
            this.button_pow.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_pow.Location = new System.Drawing.Point(386, 19);
            this.button_pow.Name = "button_pow";
            this.button_pow.Size = new System.Drawing.Size(70, 70);
            this.button_pow.TabIndex = 24;
            this.button_pow.Text = "x^n";
            this.button_pow.UseVisualStyleBackColor = false;
            this.button_pow.Click += new System.EventHandler(this.button_pow_Click);
            // 
            // label_dec
            // 
            this.label_dec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_dec.AutoSize = true;
            this.label_dec.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label_dec.Location = new System.Drawing.Point(6, 44);
            this.label_dec.Name = "label_dec";
            this.label_dec.Size = new System.Drawing.Size(24, 25);
            this.label_dec.TabIndex = 28;
            this.label_dec.Text = "0";
            // 
            // binary
            // 
            this.binary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.binary.AutoSize = true;
            this.binary.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.binary.Location = new System.Drawing.Point(6, 120);
            this.binary.Name = "binary";
            this.binary.Size = new System.Drawing.Size(24, 25);
            this.binary.TabIndex = 29;
            this.binary.Text = "0";
            // 
            // octo
            // 
            this.octo.AutoSize = true;
            this.octo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.octo.Location = new System.Drawing.Point(6, 200);
            this.octo.Name = "octo";
            this.octo.Size = new System.Drawing.Size(24, 25);
            this.octo.TabIndex = 30;
            this.octo.Text = "0";
            // 
            // hex
            // 
            this.hex.AutoSize = true;
            this.hex.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.hex.Location = new System.Drawing.Point(6, 278);
            this.hex.Name = "hex";
            this.hex.Size = new System.Drawing.Size(24, 25);
            this.hex.TabIndex = 31;
            this.hex.Text = "0";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label1.Location = new System.Drawing.Point(6, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 25);
            this.label1.TabIndex = 32;
            this.label1.Text = "Decimal : ";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label2.Location = new System.Drawing.Point(6, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 25);
            this.label2.TabIndex = 33;
            this.label2.Text = "Binary : ";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label3.Location = new System.Drawing.Point(5, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 25);
            this.label3.TabIndex = 34;
            this.label3.Text = "Octo :";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label4.Location = new System.Drawing.Point(5, 245);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 25);
            this.label4.TabIndex = 35;
            this.label4.Text = "Hex : ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gray;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label_dec);
            this.groupBox1.Controls.Add(this.hex);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.octo);
            this.groupBox1.Controls.Add(this.binary);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(474, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 327);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Системы счисления";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_1);
            this.groupBox2.Controls.Add(this.button_2);
            this.groupBox2.Controls.Add(this.button_3);
            this.groupBox2.Controls.Add(this.button_4);
            this.groupBox2.Controls.Add(this.button_5);
            this.groupBox2.Controls.Add(this.button_log10);
            this.groupBox2.Controls.Add(this.button_6);
            this.groupBox2.Controls.Add(this.button_sqrt);
            this.groupBox2.Controls.Add(this.button_7);
            this.groupBox2.Controls.Add(this.button_fact);
            this.groupBox2.Controls.Add(this.button_8);
            this.groupBox2.Controls.Add(this.button_pow);
            this.groupBox2.Controls.Add(this.button_9);
            this.groupBox2.Controls.Add(this.button_ctg);
            this.groupBox2.Controls.Add(this.button_plus);
            this.groupBox2.Controls.Add(this.button_tn);
            this.groupBox2.Controls.Add(this.button_min);
            this.groupBox2.Controls.Add(this.button_cos);
            this.groupBox2.Controls.Add(this.button_del);
            this.groupBox2.Controls.Add(this.button_sin);
            this.groupBox2.Controls.Add(this.button_0);
            this.groupBox2.Controls.Add(this.button_c);
            this.groupBox2.Controls.Add(this.button_umn);
            this.groupBox2.Controls.Add(this.button_res);
            this.groupBox2.Location = new System.Drawing.Point(6, 59);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(462, 327);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Действия";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(9, 31);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(839, 418);
            this.tabControl1.TabIndex = 38;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gray;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.textBox_numbers);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(831, 392);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Действия";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gray;
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.groupBox10);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(831, 392);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Конвертер";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(248, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(320, 37);
            this.label5.TabIndex = 9;
            this.label5.Text = "Конвертация велечин";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.button_H);
            this.groupBox10.Controls.Add(this.listBox_H1);
            this.groupBox10.Controls.Add(this.textBox_H2);
            this.groupBox10.Controls.Add(this.textBox_H1);
            this.groupBox10.Controls.Add(this.listBox_H2);
            this.groupBox10.Location = new System.Drawing.Point(625, 230);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(199, 156);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Вес";
            // 
            // button_H
            // 
            this.button_H.Location = new System.Drawing.Point(6, 121);
            this.button_H.Name = "button_H";
            this.button_H.Size = new System.Drawing.Size(182, 29);
            this.button_H.TabIndex = 4;
            this.button_H.Text = "Конвертировать";
            this.button_H.UseVisualStyleBackColor = true;
            this.button_H.Click += new System.EventHandler(this.button_H_Click);
            // 
            // listBox_H1
            // 
            this.listBox_H1.FormattingEnabled = true;
            this.listBox_H1.Items.AddRange(new object[] {
            "Граммы",
            "Килограммы"});
            this.listBox_H1.Location = new System.Drawing.Point(6, 19);
            this.listBox_H1.Name = "listBox_H1";
            this.listBox_H1.Size = new System.Drawing.Size(88, 69);
            this.listBox_H1.TabIndex = 0;
            // 
            // textBox_H2
            // 
            this.textBox_H2.Location = new System.Drawing.Point(100, 95);
            this.textBox_H2.Name = "textBox_H2";
            this.textBox_H2.Size = new System.Drawing.Size(88, 20);
            this.textBox_H2.TabIndex = 3;
            // 
            // textBox_H1
            // 
            this.textBox_H1.Location = new System.Drawing.Point(6, 95);
            this.textBox_H1.Name = "textBox_H1";
            this.textBox_H1.Size = new System.Drawing.Size(88, 20);
            this.textBox_H1.TabIndex = 1;
            // 
            // listBox_H2
            // 
            this.listBox_H2.FormattingEnabled = true;
            this.listBox_H2.Items.AddRange(new object[] {
            "Граммы",
            "Килограммы"});
            this.listBox_H2.Location = new System.Drawing.Point(100, 19);
            this.listBox_H2.Name = "listBox_H2";
            this.listBox_H2.Size = new System.Drawing.Size(88, 69);
            this.listBox_H2.TabIndex = 2;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.button_F);
            this.groupBox9.Controls.Add(this.listBox_F1);
            this.groupBox9.Controls.Add(this.textBox_F2);
            this.groupBox9.Controls.Add(this.textBox_F1);
            this.groupBox9.Controls.Add(this.listBox_F2);
            this.groupBox9.Location = new System.Drawing.Point(420, 230);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(199, 156);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Сила";
            // 
            // button_F
            // 
            this.button_F.Location = new System.Drawing.Point(6, 121);
            this.button_F.Name = "button_F";
            this.button_F.Size = new System.Drawing.Size(182, 29);
            this.button_F.TabIndex = 4;
            this.button_F.Text = "Конвертировать";
            this.button_F.UseVisualStyleBackColor = true;
            this.button_F.Click += new System.EventHandler(this.button_F_Click);
            // 
            // listBox_F1
            // 
            this.listBox_F1.FormattingEnabled = true;
            this.listBox_F1.Items.AddRange(new object[] {
            "Ньютон",
            "Килоньютон",
            "Меганьютон"});
            this.listBox_F1.Location = new System.Drawing.Point(6, 19);
            this.listBox_F1.Name = "listBox_F1";
            this.listBox_F1.Size = new System.Drawing.Size(88, 69);
            this.listBox_F1.TabIndex = 0;
            // 
            // textBox_F2
            // 
            this.textBox_F2.Location = new System.Drawing.Point(100, 95);
            this.textBox_F2.Name = "textBox_F2";
            this.textBox_F2.Size = new System.Drawing.Size(88, 20);
            this.textBox_F2.TabIndex = 3;
            // 
            // textBox_F1
            // 
            this.textBox_F1.Location = new System.Drawing.Point(6, 95);
            this.textBox_F1.Name = "textBox_F1";
            this.textBox_F1.Size = new System.Drawing.Size(88, 20);
            this.textBox_F1.TabIndex = 1;
            // 
            // listBox_F2
            // 
            this.listBox_F2.FormattingEnabled = true;
            this.listBox_F2.Items.AddRange(new object[] {
            "Ньютон",
            "Килоньютон",
            "Меганьютон"});
            this.listBox_F2.Location = new System.Drawing.Point(100, 19);
            this.listBox_F2.Name = "listBox_F2";
            this.listBox_F2.Size = new System.Drawing.Size(88, 69);
            this.listBox_F2.TabIndex = 2;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.button_grad);
            this.groupBox8.Controls.Add(this.listBox_G1);
            this.groupBox8.Controls.Add(this.textBox_G2);
            this.groupBox8.Controls.Add(this.textBox_G1);
            this.groupBox8.Controls.Add(this.listBox_G2);
            this.groupBox8.Location = new System.Drawing.Point(215, 230);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(199, 156);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Градусы";
            // 
            // button_grad
            // 
            this.button_grad.Location = new System.Drawing.Point(6, 121);
            this.button_grad.Name = "button_grad";
            this.button_grad.Size = new System.Drawing.Size(182, 29);
            this.button_grad.TabIndex = 4;
            this.button_grad.Text = "Конвертировать";
            this.button_grad.UseVisualStyleBackColor = true;
            this.button_grad.Click += new System.EventHandler(this.button_grad_Click);
            // 
            // listBox_G1
            // 
            this.listBox_G1.FormattingEnabled = true;
            this.listBox_G1.Items.AddRange(new object[] {
            "Цельсий",
            "Форенгейт"});
            this.listBox_G1.Location = new System.Drawing.Point(6, 19);
            this.listBox_G1.Name = "listBox_G1";
            this.listBox_G1.Size = new System.Drawing.Size(88, 69);
            this.listBox_G1.TabIndex = 0;
            // 
            // textBox_G2
            // 
            this.textBox_G2.Location = new System.Drawing.Point(100, 95);
            this.textBox_G2.Name = "textBox_G2";
            this.textBox_G2.Size = new System.Drawing.Size(88, 20);
            this.textBox_G2.TabIndex = 3;
            // 
            // textBox_G1
            // 
            this.textBox_G1.Location = new System.Drawing.Point(6, 95);
            this.textBox_G1.Name = "textBox_G1";
            this.textBox_G1.Size = new System.Drawing.Size(88, 20);
            this.textBox_G1.TabIndex = 1;
            // 
            // listBox_G2
            // 
            this.listBox_G2.FormattingEnabled = true;
            this.listBox_G2.Items.AddRange(new object[] {
            "Цельсий",
            "Форенгейт"});
            this.listBox_G2.Location = new System.Drawing.Point(100, 19);
            this.listBox_G2.Name = "listBox_G2";
            this.listBox_G2.Size = new System.Drawing.Size(88, 69);
            this.listBox_G2.TabIndex = 2;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button_v);
            this.groupBox7.Controls.Add(this.listBox_V1);
            this.groupBox7.Controls.Add(this.textBox_VV2);
            this.groupBox7.Controls.Add(this.textBox_VV1);
            this.groupBox7.Controls.Add(this.listBox_V2);
            this.groupBox7.Location = new System.Drawing.Point(10, 230);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(199, 156);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Объем";
            // 
            // button_v
            // 
            this.button_v.Location = new System.Drawing.Point(6, 121);
            this.button_v.Name = "button_v";
            this.button_v.Size = new System.Drawing.Size(182, 29);
            this.button_v.TabIndex = 4;
            this.button_v.Text = "Конвертировать";
            this.button_v.UseVisualStyleBackColor = true;
            this.button_v.Click += new System.EventHandler(this.button_v_Click);
            // 
            // listBox_V1
            // 
            this.listBox_V1.FormattingEnabled = true;
            this.listBox_V1.Items.AddRange(new object[] {
            "Миллилитры",
            "Литры"});
            this.listBox_V1.Location = new System.Drawing.Point(6, 19);
            this.listBox_V1.Name = "listBox_V1";
            this.listBox_V1.Size = new System.Drawing.Size(88, 69);
            this.listBox_V1.TabIndex = 0;
            // 
            // textBox_VV2
            // 
            this.textBox_VV2.Location = new System.Drawing.Point(100, 95);
            this.textBox_VV2.Name = "textBox_VV2";
            this.textBox_VV2.Size = new System.Drawing.Size(88, 20);
            this.textBox_VV2.TabIndex = 3;
            // 
            // textBox_VV1
            // 
            this.textBox_VV1.Location = new System.Drawing.Point(6, 95);
            this.textBox_VV1.Name = "textBox_VV1";
            this.textBox_VV1.Size = new System.Drawing.Size(88, 20);
            this.textBox_VV1.TabIndex = 1;
            // 
            // listBox_V2
            // 
            this.listBox_V2.FormattingEnabled = true;
            this.listBox_V2.Items.AddRange(new object[] {
            "Миллилитры",
            "Литры"});
            this.listBox_V2.Location = new System.Drawing.Point(100, 19);
            this.listBox_V2.Name = "listBox_V2";
            this.listBox_V2.Size = new System.Drawing.Size(88, 69);
            this.listBox_V2.TabIndex = 2;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button_speed);
            this.groupBox6.Controls.Add(this.listBox__U1);
            this.groupBox6.Controls.Add(this.textBox_U2);
            this.groupBox6.Controls.Add(this.textBox_U1);
            this.groupBox6.Controls.Add(this.listBox__U2);
            this.groupBox6.Location = new System.Drawing.Point(420, 68);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(199, 156);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Скорость";
            // 
            // button_speed
            // 
            this.button_speed.Location = new System.Drawing.Point(6, 121);
            this.button_speed.Name = "button_speed";
            this.button_speed.Size = new System.Drawing.Size(182, 29);
            this.button_speed.TabIndex = 4;
            this.button_speed.Text = "Конвертировать";
            this.button_speed.UseVisualStyleBackColor = true;
            this.button_speed.Click += new System.EventHandler(this.button_speed_Click);
            // 
            // listBox__U1
            // 
            this.listBox__U1.FormattingEnabled = true;
            this.listBox__U1.Items.AddRange(new object[] {
            "Км в час",
            "Милли в час"});
            this.listBox__U1.Location = new System.Drawing.Point(6, 19);
            this.listBox__U1.Name = "listBox__U1";
            this.listBox__U1.Size = new System.Drawing.Size(88, 69);
            this.listBox__U1.TabIndex = 0;
            // 
            // textBox_U2
            // 
            this.textBox_U2.Location = new System.Drawing.Point(100, 94);
            this.textBox_U2.Name = "textBox_U2";
            this.textBox_U2.Size = new System.Drawing.Size(88, 20);
            this.textBox_U2.TabIndex = 3;
            // 
            // textBox_U1
            // 
            this.textBox_U1.Location = new System.Drawing.Point(6, 95);
            this.textBox_U1.Name = "textBox_U1";
            this.textBox_U1.Size = new System.Drawing.Size(88, 20);
            this.textBox_U1.TabIndex = 1;
            // 
            // listBox__U2
            // 
            this.listBox__U2.FormattingEnabled = true;
            this.listBox__U2.Items.AddRange(new object[] {
            "Км в час",
            "Милли в час"});
            this.listBox__U2.Location = new System.Drawing.Point(100, 19);
            this.listBox__U2.Name = "listBox__U2";
            this.listBox__U2.Size = new System.Drawing.Size(88, 69);
            this.listBox__U2.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button_time);
            this.groupBox5.Controls.Add(this.listBox_T1);
            this.groupBox5.Controls.Add(this.textBox_T2);
            this.groupBox5.Controls.Add(this.textBox_T1);
            this.groupBox5.Controls.Add(this.listBox_T2);
            this.groupBox5.Location = new System.Drawing.Point(215, 68);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(199, 156);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Время";
            // 
            // button_time
            // 
            this.button_time.Location = new System.Drawing.Point(6, 121);
            this.button_time.Name = "button_time";
            this.button_time.Size = new System.Drawing.Size(182, 29);
            this.button_time.TabIndex = 4;
            this.button_time.Text = "Конвертировать";
            this.button_time.UseVisualStyleBackColor = true;
            this.button_time.Click += new System.EventHandler(this.button_time_Click);
            // 
            // listBox_T1
            // 
            this.listBox_T1.FormattingEnabled = true;
            this.listBox_T1.Items.AddRange(new object[] {
            "Секунды",
            "Минуты",
            "Часы"});
            this.listBox_T1.Location = new System.Drawing.Point(6, 19);
            this.listBox_T1.Name = "listBox_T1";
            this.listBox_T1.Size = new System.Drawing.Size(88, 69);
            this.listBox_T1.TabIndex = 0;
            // 
            // textBox_T2
            // 
            this.textBox_T2.Location = new System.Drawing.Point(100, 95);
            this.textBox_T2.Name = "textBox_T2";
            this.textBox_T2.Size = new System.Drawing.Size(88, 20);
            this.textBox_T2.TabIndex = 3;
            // 
            // textBox_T1
            // 
            this.textBox_T1.Location = new System.Drawing.Point(6, 95);
            this.textBox_T1.Name = "textBox_T1";
            this.textBox_T1.Size = new System.Drawing.Size(88, 20);
            this.textBox_T1.TabIndex = 1;
            // 
            // listBox_T2
            // 
            this.listBox_T2.FormattingEnabled = true;
            this.listBox_T2.Items.AddRange(new object[] {
            "Секунды",
            "Минуты",
            "Часы"});
            this.listBox_T2.Location = new System.Drawing.Point(100, 20);
            this.listBox_T2.Name = "listBox_T2";
            this.listBox_T2.Size = new System.Drawing.Size(88, 69);
            this.listBox_T2.TabIndex = 2;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button_val);
            this.groupBox4.Controls.Add(this.listBox__VAL1);
            this.groupBox4.Controls.Add(this.textBox_V2);
            this.groupBox4.Controls.Add(this.textBox_V1);
            this.groupBox4.Controls.Add(this.listBox__VAL2);
            this.groupBox4.Location = new System.Drawing.Point(625, 68);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(199, 159);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Валюта";
            // 
            // button_val
            // 
            this.button_val.Location = new System.Drawing.Point(6, 121);
            this.button_val.Name = "button_val";
            this.button_val.Size = new System.Drawing.Size(182, 29);
            this.button_val.TabIndex = 5;
            this.button_val.Text = "Конвертировать";
            this.button_val.UseVisualStyleBackColor = true;
            this.button_val.Click += new System.EventHandler(this.button_val_Click);
            // 
            // listBox__VAL1
            // 
            this.listBox__VAL1.FormattingEnabled = true;
            this.listBox__VAL1.Items.AddRange(new object[] {
            "Евро",
            "Доллар",
            "Рубль"});
            this.listBox__VAL1.Location = new System.Drawing.Point(6, 19);
            this.listBox__VAL1.Name = "listBox__VAL1";
            this.listBox__VAL1.Size = new System.Drawing.Size(88, 69);
            this.listBox__VAL1.TabIndex = 0;
            // 
            // textBox_V2
            // 
            this.textBox_V2.Location = new System.Drawing.Point(100, 95);
            this.textBox_V2.Name = "textBox_V2";
            this.textBox_V2.Size = new System.Drawing.Size(88, 20);
            this.textBox_V2.TabIndex = 3;
            // 
            // textBox_V1
            // 
            this.textBox_V1.Location = new System.Drawing.Point(6, 95);
            this.textBox_V1.Name = "textBox_V1";
            this.textBox_V1.Size = new System.Drawing.Size(88, 20);
            this.textBox_V1.TabIndex = 1;
            // 
            // listBox__VAL2
            // 
            this.listBox__VAL2.FormattingEnabled = true;
            this.listBox__VAL2.Items.AddRange(new object[] {
            "Евро",
            "Доллар",
            "Рубль"});
            this.listBox__VAL2.Location = new System.Drawing.Point(100, 19);
            this.listBox__VAL2.Name = "listBox__VAL2";
            this.listBox__VAL2.Size = new System.Drawing.Size(88, 69);
            this.listBox__VAL2.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_s);
            this.groupBox3.Controls.Add(this.listBox_S1);
            this.groupBox3.Controls.Add(this.textBox_S2);
            this.groupBox3.Controls.Add(this.textBox_S1);
            this.groupBox3.Controls.Add(this.listBox_S2);
            this.groupBox3.Location = new System.Drawing.Point(10, 68);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(199, 156);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Расстояние";
            // 
            // button_s
            // 
            this.button_s.Location = new System.Drawing.Point(6, 121);
            this.button_s.Name = "button_s";
            this.button_s.Size = new System.Drawing.Size(182, 29);
            this.button_s.TabIndex = 4;
            this.button_s.Text = "Конвертировать";
            this.button_s.UseVisualStyleBackColor = true;
            this.button_s.Click += new System.EventHandler(this.button_s_Click);
            // 
            // listBox_S1
            // 
            this.listBox_S1.FormattingEnabled = true;
            this.listBox_S1.Items.AddRange(new object[] {
            "Сантиметры",
            "Дециметры",
            "Метры",
            "Километры"});
            this.listBox_S1.Location = new System.Drawing.Point(6, 19);
            this.listBox_S1.Name = "listBox_S1";
            this.listBox_S1.Size = new System.Drawing.Size(88, 69);
            this.listBox_S1.TabIndex = 0;
            // 
            // textBox_S2
            // 
            this.textBox_S2.Location = new System.Drawing.Point(100, 95);
            this.textBox_S2.Name = "textBox_S2";
            this.textBox_S2.Size = new System.Drawing.Size(88, 20);
            this.textBox_S2.TabIndex = 3;
            // 
            // textBox_S1
            // 
            this.textBox_S1.Location = new System.Drawing.Point(6, 95);
            this.textBox_S1.Name = "textBox_S1";
            this.textBox_S1.Size = new System.Drawing.Size(88, 20);
            this.textBox_S1.TabIndex = 1;
            // 
            // listBox_S2
            // 
            this.listBox_S2.FormattingEnabled = true;
            this.listBox_S2.Items.AddRange(new object[] {
            "Сантиметры",
            "Дециметры",
            "Метры",
            "Километры"});
            this.listBox_S2.Location = new System.Drawing.Point(100, 19);
            this.listBox_S2.Name = "listBox_S2";
            this.listBox_S2.Size = new System.Drawing.Size(88, 69);
            this.listBox_S2.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Gray;
            this.tabPage3.Controls.Add(this.richTextBox_text);
            this.tabPage3.Controls.Add(this.button_save);
            this.tabPage3.Controls.Add(this.button_open);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(831, 392);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Запись промежуточных результатов";
            // 
            // richTextBox_text
            // 
            this.richTextBox_text.Location = new System.Drawing.Point(3, 3);
            this.richTextBox_text.Name = "richTextBox_text";
            this.richTextBox_text.Size = new System.Drawing.Size(824, 344);
            this.richTextBox_text.TabIndex = 4;
            this.richTextBox_text.Text = "";
            // 
            // button_save
            // 
            this.button_save.BackColor = System.Drawing.Color.White;
            this.button_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_save.Location = new System.Drawing.Point(731, 353);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(96, 36);
            this.button_save.TabIndex = 3;
            this.button_save.Text = "Сохранить";
            this.button_save.UseVisualStyleBackColor = false;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // button_open
            // 
            this.button_open.BackColor = System.Drawing.Color.White;
            this.button_open.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_open.Location = new System.Drawing.Point(629, 353);
            this.button_open.Name = "button_open";
            this.button_open.Size = new System.Drawing.Size(96, 36);
            this.button_open.TabIndex = 2;
            this.button_open.Text = "Открыть";
            this.button_open.UseVisualStyleBackColor = false;
            this.button_open.Click += new System.EventHandler(this.button_open_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(860, 459);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "Калькулятор бездельника";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label_close;
        private System.Windows.Forms.Button button_1;
        private System.Windows.Forms.Button button_2;
        private System.Windows.Forms.Button button_3;
        private System.Windows.Forms.Button button_4;
        private System.Windows.Forms.Button button_5;
        private System.Windows.Forms.Button button_6;
        private System.Windows.Forms.Button button_7;
        private System.Windows.Forms.Button button_8;
        private System.Windows.Forms.Button button_9;
        private System.Windows.Forms.Button button_plus;
        private System.Windows.Forms.Button button_min;
        private System.Windows.Forms.Button button_del;
        private System.Windows.Forms.Button button_0;
        private System.Windows.Forms.Button button_umn;
        private System.Windows.Forms.Button button_res;
        private System.Windows.Forms.Button button_c;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox textBox_numbers;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.Button button_tn;
        private System.Windows.Forms.Button button_ctg;
        private System.Windows.Forms.Button button_log10;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Button button_fact;
        private System.Windows.Forms.Button button_pow;
        private System.Windows.Forms.Label label_dec;
        private System.Windows.Forms.Label binary;
        private System.Windows.Forms.Label octo;
        private System.Windows.Forms.Label hex;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_hide;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox listBox_S1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button_speed;
        private System.Windows.Forms.ListBox listBox__U1;
        private System.Windows.Forms.TextBox textBox_U2;
        private System.Windows.Forms.TextBox textBox_U1;
        private System.Windows.Forms.ListBox listBox__U2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button_time;
        private System.Windows.Forms.ListBox listBox_T1;
        private System.Windows.Forms.TextBox textBox_T2;
        private System.Windows.Forms.TextBox textBox_T1;
        private System.Windows.Forms.ListBox listBox_T2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button_val;
        private System.Windows.Forms.ListBox listBox__VAL1;
        private System.Windows.Forms.TextBox textBox_V2;
        private System.Windows.Forms.TextBox textBox_V1;
        private System.Windows.Forms.ListBox listBox__VAL2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_s;
        private System.Windows.Forms.TextBox textBox_S2;
        private System.Windows.Forms.TextBox textBox_S1;
        private System.Windows.Forms.ListBox listBox_S2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button button_H;
        private System.Windows.Forms.ListBox listBox_H1;
        private System.Windows.Forms.TextBox textBox_H2;
        private System.Windows.Forms.TextBox textBox_H1;
        private System.Windows.Forms.ListBox listBox_H2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button button_F;
        private System.Windows.Forms.ListBox listBox_F1;
        private System.Windows.Forms.TextBox textBox_F2;
        private System.Windows.Forms.TextBox textBox_F1;
        private System.Windows.Forms.ListBox listBox_F2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button_grad;
        private System.Windows.Forms.ListBox listBox_G1;
        private System.Windows.Forms.TextBox textBox_G2;
        private System.Windows.Forms.TextBox textBox_G1;
        private System.Windows.Forms.ListBox listBox_G2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button_v;
        private System.Windows.Forms.ListBox listBox_V1;
        private System.Windows.Forms.TextBox textBox_VV2;
        private System.Windows.Forms.TextBox textBox_VV1;
        private System.Windows.Forms.ListBox listBox_V2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Button button_open;
        private System.Windows.Forms.RichTextBox richTextBox_text;
    }
}

